# Praca Domowa 4

Zadanie polega na utworzeniu w katalogu www strony wizytówki. Sposób wykonania jest dowolony, tak samo jak i jej wygląd. 

Wymagane elementy:

    1. portfolio: po kliknięciu powinno wyskoczyć okienko (modal) z informacjami szczegółowymi
    2. formularz kontaktowy wraz z walidacją i wysyłany metodą POST na adres "/form", z polami:
        1. Imię
        2. Nazwisko
        3. Numer Telefonu
        4. Email

Należy pobrać pliki do katalogu ze swoim repozytorium z pracą domową, oraz uruchomić w terminalu "npm install".
Do uruchomienia projektu używamy polecenia w terminalu "node index.js"