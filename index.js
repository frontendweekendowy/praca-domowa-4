const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

var parser = bodyParser.urlencoded({extended: false});

app.use(express.static(path.join(__dirname, 'www')));

app.get('/', function (req, res) {
  res.sendFile('www/index.html')
})

app.post('/form', parser, function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(req.body));
})

app.listen(3000, function () {
  console.log('Adres w przeglądarce: http://localhost:3000')
})